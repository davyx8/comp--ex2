function I = getLaplacian(Gx,Gy)

% get image size:
[h,w] = size(Gx);

% create the div of the image:
div = sparse(double(imfilter(Gx, [-1 1 0],0) + imfilter(Gy, [-1 1 0]',0)));

% create the laplacian matrix:
ex = ones(h,1);
% Laplacian in the x-direction 
Dxx = spdiags([ex -2*ex ex], [-1 0 1], h, h);
ey = ones(w,1);
%  Laplacian in the y-direction ;
Dyy = spdiags([ey, -2*ey ey], [-1 0 1], w, w); 
% Kronecker tensor product, so we will have the approximation for each
% pixel
L = kron(Dyy, speye(h)) + kron(speye(w), Dxx) ;

% retrieve original image:
I = L\div(:);
I = full(reshape(I,h,w));
end