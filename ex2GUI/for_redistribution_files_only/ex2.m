function [fixedImage ] =ex2(filename,LorH,alpha,beta,gamma,method)
if( LorH ==1)
    im = hdrread(filename);

elseif LorH==0
    im = im2double(imread(filename));

end
gray = rgb2gray((im));
gray = gray - min(gray(:));
gray = gray/max(gray(:));
gray= gray* 100;
loggray = log(gray+1.1921e-07);


%getting gau pyramid
[h, w] = size(loggray);
% display(h)
% display(w)
im2 = floor(min(log2(h/32),log2(w/32)));
pyr = cell(im2+1,1);
pyr{1} = loggray;

for i=2:im2+1
    pyr{i} = imresize(pyr{i-1},0.5,'bilinear');
end

%getting the deriviative

H = cell(numel(pyr),1);

for j=1:numel(pyr)
    x = conv2(pyr{j},[1 0 -1],'same')/2^j;
    y = conv2(pyr{j},[1 0 -1]','same')/2^j;
    H{j} = sqrt(x.^2 + y.^2);
end
Phi=getAttenDeriv(H,alpha,beta,method);
% imshow(Phi);
% Preparing the G for the Possion equasion
[Gx, Gy] = imgradientxy(loggray,'IntermediateDifference');
Gx = Gx .* Phi;
Gy = Gy .* Phi;

% solve the poisson equation:
I = getLaplacian(Gx,Gy);

% take exponent (to cancel the LOG)
I = exp(I);
% normalize between [0,1]

meanMax = max(I(:));
meanMin = min(I(:));
I = I-meanMin;
I = I/meanMax;

% Adding the color to the gray scale
gIn = rgb2gray(im);
Lin = cat(3,gIn,gIn,gIn);
Lout = cat(3,I,I,I);
RGB = (im./Lin);
RGB = RGB .* Lout;

fixedImage  = RGB.^(1/gamma);
imwrite(fixedImage, 'fixedImage.png');

end
