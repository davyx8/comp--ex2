function result = getAttenDeriv(H,alpha,beta,method)
n = length(H);
Phi = cell(n,1);
if (method==1)
    for i=1:n
        im=H{i};
        im(im<=0) = 1.1921e-07;
        Phi{i}  = (im./alpha).^(beta-1);
%         if(i==1)
%             figure;
%             imshow(Phi{i});
%         end
        
    end
    
    
    
elseif (method==0)
    for i=1:n
        im=H{i};
        [h,w] = size(im);
        
        im(im<=0) = 1.1921e-07;
        if(i==1)

                figure;
                surf(double(im));
        end
        %         figure;
        %         imshow(im);
        im(im<=0) = 1.1921e-07;
        [XMAX,IMAX,XMIN,IMIN] = extrema2(double(im(2:h-1,2:w-1)));
        
        aveMax = median(XMAX);
        aveMin = median(XMIN);     
        ratio = aveMax/aveMin;

            im(im>=aveMax*alpha) = im(im>=aveMax*alpha)/(ratio*beta);
       negative = 1-im;
       negative(negative<=0) = 1.1921e-07;
        if(i==1)
%             figure;
%             imshow(negative);
                            figure;
                surf(double(im));
        end
        
        
        %         imshow(1-im);
        Phi{i}  = negative;
    end
    
    
end
% Sum the pyramid
for i=n-1:-1:1
    Phi{i} = imresize(Phi{i+1},size(Phi{i}),'bilinear' ).* Phi{i};
end
result = Phi{1};
%imagesc(result)
end

