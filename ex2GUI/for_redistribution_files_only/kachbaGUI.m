function varargout = kachbaGUI(varargin)
% KACHBAGUI MATLAB code for kachbaGUI.fig
%      KACHBAGUI, by itself, creates a new KACHBAGUI or raises the existing
%      singleton*.
%
%      H = KACHBAGUI returns the handle to a new KACHBAGUI or the handle to
%      the existing singleton*.
%
%      KACHBAGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in KACHBAGUI.M with the given input arguments.
%
%      KACHBAGUI('Property','Value',...) creates a new KACHBAGUI or raises
%      the existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before kachbaGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to kachbaGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help kachbaGUI

% Last Modified by GUIDE v2.5 27-May-2015 16:52:05

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @kachbaGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @kachbaGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before kachbaGUI is made visible.
function kachbaGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to kachbaGUI (see VARARGIN)

% Choose default command line output for kachbaGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
set(handles.filename,'String','');
set(handles.fullfile,'String','');
set(handles.alpha,'Value',0.5);
set(handles.beta,'Value',0.5);

set(handles.gamma,'Value',0.5);

set(handles.pic ,'Visible','off');
set(handles.out ,'Visible','off');


initialize_gui(hObject, handles, false);

% UIWAIT makes kachbaGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = kachbaGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function alpha_CreateFcn(hObject, eventdata, handles)
% hObject    handle to alpha (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function alpha_Callback(hObject, eventdata, handles)
% hObject    handle to alpha (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of alpha as text
%        str2double(get(hObject,'String')) returns contents of alpha as a double
alpha = str2double(get(hObject, 'String'));
if isnan(alpha)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end

% Save the new alpha value
handles.metricdata.alpha = alpha;
set(handles.alpha,'Value',alpha);
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function beta_CreateFcn(hObject, eventdata, handles)
% hObject    handle to beta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function beta_Callback(hObject, eventdata, handles)
% hObject    handle to beta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of beta as text
%        str2double(get(hObject,'String')) returns contents of beta as a double
beta = str2double(get(hObject, 'String'));
if isnan(beta)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end

set(handles.beta,'Value',beta);
% Save the new beta value
handles.metricdata.beta = beta;
guidata(hObject,handles)

% --- Executes on button press in calculate.
function calculate_Callback(hObject, eventdata, handles)
% hObject    handle to calculate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

filestring= get(handles.fullfile,'String');
if strcmp(filestring,'EMPTY')
    h = msgbox('please load an image');
    return
end

filename= get(handles.filename,'String');
filestring= get(handles.fullfile,'String');
%hdr = get(handles.rdbHDR,'Value');

alpha = get(handles.alpha,'Value');
beta = get(handles.beta,'Value');
gamma = get(handles.gamma,'Value');
HorL =get(handles.radiobutton9,'Value');
met =get(handles.radiobutton11,'Value');
display(filestring)
display(alpha)
display(beta)

display(HorL)

display(gamma);
res=ex2(filename,HorL,alpha,beta,gamma, met);
axes(handles.out);
imshow(res)

% --- Executes on button press in upload.
function upload_Callback(hObject, eventdata, handles)
% hObject    handle to upload (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

initialize_gui(gcbf, handles, true);
display('kachba2 ')


opt = get(handles.radiobutton9,'Value');
if opt == 1
    [name,dir] = uigetfile('*.hdr;');
else
    [name,dir] = uigetfile('*.jpg;*.jpeg');
end
display(name)
display(dir)


%if (all(name ~= 0) && all(dir ~= 0))
    set(handles.fullfile,'String',fullfile(dir,name))
    display(handles.fullfile)
    set(handles.filename,'String',[dir name])
%end

if opt == 1
im=hdrread([dir name])
else
im=imread([dir name])
end
axes(handles.pic);
imshow(im)
axes(handles.out);
% --- Executes when selected object changed in unitgroup.
function unitgroup_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in unitgroup 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if (hObject == handles.HDR)

else

end

% --------------------------------------------------------------------
function initialize_gui(fig_handle, handles, isreset)
% If the metricdata field is present and the upload flag is false, it means
% we are we are just re-initializing a GUI by calling it from the cmd line
% while it is up. So, bail out as we dont want to upload the data.
if isfield(handles, 'metricdata') && ~isreset
    return;
end

handles.metricdata.alpha = 0;
handles.metricdata.beta  = 0;
handles.metricdata.gamma  = 0;

set(handles.alpha, 'String', handles.metricdata.alpha);
set(handles.beta,  'String', handles.metricdata.beta);
set(handles.gamma,  'String', handles.metricdata.gamma);

set(handles.unitgroup, 'SelectedObject', handles.HDR);

% Update handles structure
guidata(handles.figure1, handles);



function gamma_Callback(hObject, eventdata, handles)
% hObject    handle to gamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of gamma as text
%        str2double(get(hObject,'String')) returns contents of gamma as a double
gamma = str2double(get(hObject, 'String'));
if isnan(gamma)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end

set(handles.gamma,'Value',gamma);

% --- Executes during object creation, after setting all properties.
function gamma_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over upload.
function upload_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to upload (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
display('kachba ')



% --- Executes on button press in radiobutton10.
function radiobutton10_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton10


% --- Executes on button press in radiobutton9.
function radiobutton9_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton9
